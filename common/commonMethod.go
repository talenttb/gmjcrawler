package common

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"../config/logConfig"
)

func init() {

}

func GetSite(uri string) (result string) {
	fmt.Println("Download Page. Site", uri)
	res, err := http.Get(uri)

	if err != nil {
		msg := fmt.Sprintf("Get url error : %s.", err)
		logConfig.Write(msg)
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)

	//logConfig.Write(fmt.Sprintf("*********************\n%s\n*********************", string(body)))

	if err != nil {
		msg := fmt.Sprintf("Get url error : %s.", err)
		logConfig.Write(msg)
	}

	result = string(body)
	return
}

//GetSiteCH get url html
func GetSiteCH(uri string, html chan string) {
	fmt.Println("Download Page. Site", uri)
	res, err := http.Get(uri)

	if err != nil {
		msg := fmt.Sprintf("Get url error : %s.", err)
		logConfig.Write(msg)
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)

	//logConfig.Write(fmt.Sprintf("*********************\n%s\n*********************", string(body)))

	if err != nil {
		msg := fmt.Sprintf("Get url error : %s.", err)
		logConfig.Write(msg)
	}

	html <- string(body)

}
