package logConfig

import (
	"log"
	"os"
)

//Write to log file
func Write(msg string) {
	f, _ := os.OpenFile("logFile.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer f.Close()
	log.SetOutput(f)
	log.Println(msg)
}
