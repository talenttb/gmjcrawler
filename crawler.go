package main

import (
	"./config/logConfig"
	"./webTarget"
)

func main() {

	logConfig.Write("start")

	webTarget.GomajiLoadData()

	logConfig.Write("done.")
}
