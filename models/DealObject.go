// DealObject
package models

type Deal struct {
	Bid string
	ItemName string
	StoreName string
	ImageUrl string
	Lat      string
	Lng      string
}
