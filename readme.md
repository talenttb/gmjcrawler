# 爬對手網站

## 目的

業務自誇某區域領先對手，但經由時地考察以及網路搜尋後，發現有出入，但沒有實際數據，所以找一個小專案來練習使用GO順便把實際數據交給主管。

## 作法

分析對方網站，然後把HTML用Regular expression轉換成Google map API所使用的數據。

開了有Buffer的Channel來做下載的非同步。

## 成果

最後是一個JSON物件陣列

    [{"Bid":"","ItemName":"只要769元起，即可享","StoreName":"特別企劃 :","ImageUrl":"","Lat":"24.6356209","Lng":"120.8759056"},...

然後使用Google map API放置座標。

概念類似 http://www.55555.tw/map/

## TODO

* fail handler