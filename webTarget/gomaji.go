package webTarget

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strings"
	"sync"

	"../common"
	log "../config/logConfig"
	"../models"
)

var domainGomaji = "http://www.gomaji.com"
var resultData = []models.Deal{}
var wg sync.WaitGroup

//GomajiLoadData parse body
func GomajiLoadData() {
	log.Write("Gomaji Load Data")
	//targetURL := "http://www.gomaji.com/index.php?city=Taipei&page="

	dealMapURL := make(map[string]string)

	//	cityList := []string{"Taipei", "Hsinchu", "Taoyuan", "Taichung", "Tainan", "Kaohsiung", "Miaoli",
	//		"Changhua", "Nantou", "Yunlin", "Chiayi", "Pingtung", "Taitung", "Hualien", "Yilan", "Keelung"}
	// chList := []int{7, 8, 9}
	//             餐廳  美容  生活

	//demo 單一區域
	cityList := []string{"Taoyuan"}
	//demo 單一管別
	chList := []int{7}

	for _, city := range cityList {
		for _, ch := range chList {
			for i := 1; ; i++ {
				targetURL := fmt.Sprintf("%s/index.php?city=%s&ch=%d&page=%d", domainGomaji, city, ch, i)
				htmlStr := common.GetSite(targetURL)
				re, _ := regexp.Compile(`img/error404.jpg`)
				//re, _ := regexp.Compile(`an error occurred`)

				if re.MatchString(htmlStr) {
					break
				}

				re, _ = regexp.Compile(city + `.*?html`)
				deals := re.FindAllStringSubmatch(htmlStr, -1)

				for _, item := range deals {
					if _, exists := dealMapURL[item[0]]; !exists {
						dealMapURL[item[0]] = ""
					}
				}
			}
		}
	}

	//旅遊檔次，DEMO先關閉
	// for i := 1; ; i++ {
	// 	targetURL := fmt.Sprintf("%s/travel.php?page=%d", domainGomaji, i)
	// 	htmlStr := common.GetSite(targetURL)
	// 	re, _ := regexp.Compile(`img/error404.jpg`)
	// 	//re,_:=regexp.Compile(`an error occurred`)
	// 	if re.MatchString(htmlStr) {
	// 		break
	// 	}
	// 	re, _ = regexp.Compile(`Travel.*?html`)
	// 	deals := re.FindAllStringSubmatch(htmlStr, -1)
	// 	for _, item := range deals {
	// 		if _, exists := dealMapURL[item[0]]; !exists {
	// 			//do something here
	// 			dealMapURL[item[0]] = ""
	// 		}
	// 	}
	// }

	htmlCh := make(chan string, 4)

	for key := range dealMapURL {
		wg.Add(1)
		go common.GetSiteCH(fmt.Sprintf("%s/%s", domainGomaji, key), htmlCh)
		go parse2Obj(key, htmlCh)
	}
	wg.Wait()

	b, err := json.Marshal(resultData)
	if err != nil {
		fmt.Println(err)
	}
	// fmt.Println(string(b))
	log.Write(string(b))
}

func parse2Obj(key string, html chan string) {
	defer wg.Done()
	htmlStr := <-html

	regAllstr := "[^<]+"
	re, _ := regexp.Compile("<span class=\"product-name\">" + regAllstr + "</span>")
	itemNameItems := re.FindAllStringSubmatch(htmlStr, -1)

	re, _ = regexp.Compile(`sll=[0-9]{1,2}\.[0-9]+,?[0-9]{1,3}\.[0-9]+`)
	locationItems := re.FindAllStringSubmatch(htmlStr, -1)

	re, _ = regexp.Compile("<label>" + regAllstr + "</label>")

	storeNameItems := re.FindAllStringSubmatch(htmlStr, -1)

	for i := 0; i < len(locationItems); i++ {
		itemName := ""
		if len(itemNameItems) > 0 {
			re, _ = regexp.Compile("<[^>]*>")
			itemName = fmt.Sprint(re.ReplaceAllLiteralString(strings.Join(itemNameItems[0], ""), ""))
		} else {
			itemName = key
		}

		re, _ = regexp.Compile("</?label>")
		storeName := ""
		if len(storeNameItems) > i+1 {
			storeName = fmt.Sprint(re.ReplaceAllLiteralString(strings.Join(storeNameItems[i+1], ""), ""))
		} else {
			storeName = fmt.Sprint(re.ReplaceAllLiteralString(strings.Join(storeNameItems[1], ""), ""))
		}

		lat := ""
		lng := ""

		if tmpStr := fmt.Sprintf("%s", locationItems[i]); strings.Contains(tmpStr, "=") {
			if tmpStr2 := strings.Split(tmpStr, "=")[1]; strings.Contains(tmpStr2, ",") {
				tmpStr3 := strings.TrimRight(tmpStr2, "]")
				lat = strings.Split(tmpStr3, ",")[0]
				lng = strings.Split(tmpStr3, ",")[1]
			}
		}

		//配合17格式
		tmp := models.Deal{
			//Bid:"",
			ItemName:  itemName,
			StoreName: storeName,
			//ImageUrl:"",
			Lat: lat,
			Lng: lng,
		}

		resultData = append(resultData, tmp)
	}
}
